package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimulacroParcial1Application {

	public static void main(String[] args) {
		SpringApplication.run(SimulacroParcial1Application.class, args);
	}

}
