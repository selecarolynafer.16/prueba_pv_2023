package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.model.Persona;

@Controller
public class PersonaController {

    private List<Persona> listaPersonas = new ArrayList<>();

    @GetMapping("/")
    public String mostrarListaPersonas(Model model) {
        model.addAttribute("personas", listaPersonas);
        return "index";
    }

    @GetMapping("/agregar")
    public String mostrarFormularioAgregar(Model model) {
        model.addAttribute("persona", new Persona());
        return "agregar";
    }

    @PostMapping("/guardar")
    public String guardarPersona(@ModelAttribute Persona persona) {
        listaPersonas.add(persona);
        return "redirect:/";
    }

    @GetMapping("/editar/{dni}")
    public String mostrarFormularioEditar(@PathVariable("dni") int dni, Model model) {
        Persona persona = buscarPersona(dni);
        if (persona != null) {
            model.addAttribute("persona", persona);
            return "editar";
        } else {
            return "redirect:/";
        }
    }

    @PostMapping("/actualizar")
    public String actualizarPersona(@ModelAttribute Persona persona) {
        Persona personaExistente = buscarPersona(persona.getDni());
        if (personaExistente != null) {
            personaExistente.setApellido(persona.getApellido());
            personaExistente.setNombre(persona.getNombre());
            personaExistente.setEdad(persona.getEdad());
        }
        return "redirect:/";
    }

    @GetMapping("/eliminar/{dni}")
    public String eliminarPersona(@PathVariable("dni") int dni) {
        Persona persona = buscarPersona(dni);
        if (persona != null) {
            listaPersonas.remove(persona);
        }
        return "redirect:/";
    }

    private Persona buscarPersona(int dni) {
        for (Persona persona : listaPersonas) {
            if (persona.getDni() == dni) {
                return persona;
            }
        }
        return null;
    }
}
